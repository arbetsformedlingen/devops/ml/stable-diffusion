#!/usr/bin/env bash
#set -eEu -o pipefail
self_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

export WORKSPACE=/workspace

# for saving vast ai envvars from the init shell
env | grep _ >> /etc/environment

trap "touch /tmp/.finished" EXIT

mkdir -p "$WORKSPACE"/in "$WORKSPACE"/out/txt2img "$WORKSPACE"/out/img2img

cd "$WORKSPACE"

curl https://gitlab.com/arbetsformedlingen/devops/ml/stable-diffusion/-/archive/main/stable-diffusion-main.tar.gz |\
    tar --strip-components=1 -zxf -

#CONDFILE=Miniconda3-py310_23.5.2-0-Linux-x86_64.sh
CONDFILE=Miniconda3-py310_23.10.0-1-Linux-x86_64.sh
wget "https://repo.anaconda.com/miniconda/$CONDFILE"
sudo bash "$CONDFILE" -b
rm -f "$CONDFILE"
export PATH=$PATH:$HOME/miniconda3/bin

sudo apt -y install libsm6 libxrender1 libxext-dev libgl1

git clone  --depth=1 https://github.com/CompVis/stable-diffusion.git
cd stable-diffusion

conda init bash
conda env create -f environment.yaml
. /root/.bashrc
. $HOME/miniconda3/etc/profile.d/conda.sh
conda activate /opt/conda/envs/ldm

curl -O -L https://huggingface.co/CompVis/stable-diffusion-v-1-4-original/resolve/main/sd-v1-4.ckpt
mkdir -p models/ldm/stable-diffusion-v1/
ln -s -r sd-v1-4.ckpt models/ldm/stable-diffusion-v1/model.ckpt

curl -o "$WORKSPACE"/in/lars_loow.jpg https://arbetsformedlingen.se/images/18.2fa8be2a1851ae3ef9c58f/1671712530770/lars_loow-5012.jpg

#--ckpt ./stable-diffusion-2-1/v2-1_768-ema-pruned.ckpt
# --H 768 --W 768 --scale 2

time python scripts/txt2img.py --from-file "$WORKSPACE"/in/txt2img_prompts0.txt --plms --ckpt sd-v1-4.ckpt --skip_grid --n_samples 1 --outdir "$WORKSPACE"/out/txt2img > "$WORKSPACE"/out/txt2img/output.txt 2>"$WORKSPACE"/out/txt2img/error.txt

time python scripts/img2img.py --from-file "$WORKSPACE"/in/img2img_prompts0.txt --init-img "$WORKSPACE"/in/lars_loow.jpg --n_samples 1 --strength 0.8 --outdir "$WORKSPACE"/out/img2img > "$WORKSPACE"/out/img2img/output.txt 2>"$WORKSPACE"/out/img2img/error.txt
